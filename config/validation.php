<?php

/**
 * Shared Validation Rules
 * 
 * @return array
 */
return [
    'product' => [
        'name' => 'required|string',
        'description' => 'nullable|string',
        'style' => 'required|string',
        'brand' => 'required|string',
        'url' => 'nullable|url',
        'shipping_price' => 'required|numeric',
        'note' => 'nullable|string',
    ],
];
