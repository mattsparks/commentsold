<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plan_id');
            $table->unsignedBigInteger('shop_id');
            $table->foreign('plan_id')->references('id')->on('plans');
            $table->foreign('shop_id')->references('id')->on('shops');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('superadmin');
            $table->boolean('is_enabled');
            $table->string('card_brand');
            $table->integer('card_last_four');
            $table->rememberToken();
            $table->timestamp('trial_starts_at')->nullable();
            $table->timestamp('trial_ends_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
