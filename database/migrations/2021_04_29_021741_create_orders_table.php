<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('inventory_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('cascade');
            $table->string('street_address');
            $table->string('apartment')->nullable();
            $table->string('city');
            $table->tinyText('state');
            $table->tinyText('country_code');
            $table->integer('zip');
            $table->string('phone_number');
            $table->string('email');
            $table->string('name');
            $table->string('order_status');
            $table->string('payment_ref');
            $table->string('transaction_id');
            $table->integer('payment_amt_cents');
            $table->integer('ship_charged_cents');
            $table->integer('ship_cost_cents');
            $table->integer('subtotal_cents');
            $table->integer('total_cents');
            $table->integer('tax_total_cents');
            $table->string('shipper_name');
            $table->string('tracking_number');
            $table->timestamp('payment_date')->nullable();
            $table->timestamp('shipped_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
