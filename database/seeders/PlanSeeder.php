<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use TheStringler\Manipulator\Manipulator as M;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = ['Startup', 'Boutique', 'Enterprise'];

        foreach ($plans as $name) {
            DB::table('plans')->insert([
                'name' => $name,
                'slug' => M::make($name)->toSlug(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
