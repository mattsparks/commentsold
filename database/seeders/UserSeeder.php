<?php

namespace Database\Seeders;

use Carbon\Carbon;
use League\Csv\Reader;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath(storage_path('csv/users.csv'), 'r');
        $csv->setHeaderOffset(0);

        $planMap = [
            'Startup' => 1,
            'Boutique' => 2,
            'Enterprise' => 3,
        ];

        foreach ($csv as $record) {
            $shopId = DB::table('shops')->insertGetId(
                [
                    'name' => $record['shop_name'],
                    'domain' => $record['shop_domain'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]
            );

            // Let's re-hash so we can use Laravel's auth stuff.
            $hashedPassword = $record['password_plain'] ? Hash::make($record['password_plain']) : null;

            DB::table('users')->insert([
                'id' => $record['id'] ?? null,
                'plan_id' => $planMap[$record['billing_plan']] ?? null,
                'shop_id' => $shopId,
                'name' => $record['name'] ?? null,
                'email' => $record['email'] ?? null,
                'password' => $hashedPassword,
                'superadmin' => $record['superadmin'] ?? null,
                'is_enabled' => $record['is_enabled'] ?? null,
                'card_brand' => $record['card_brand'] ?? null,
                'card_last_four' => $record['card_last_four'] ?? null,
                'remember_token' => $record['remember_token'] ?? null,
                'trial_starts_at' => $record['trial_starts_at'] ?? null,
                'trial_ends_at' => $record['trial_ends_at'] ?? null,
                'email_verified_at' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
