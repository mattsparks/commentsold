<?php

namespace Database\Seeders;

use Carbon\Carbon;
use League\Csv\Reader;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath(storage_path('csv/inventory.csv'), 'r');
        $csv->setHeaderOffset(0);

        foreach ($csv as $record) {

            $product = Product::find($record['product_id']);

            // Some product_id's don't exist, skip those.
            if (!$product) {
                continue;
            }

            DB::table('inventories')->insert([
                'id' => $record['id'] ?? null,
                'product_id' => $record['product_id'] ?? null,
                'quantity' => $record['quantity'] ?? null,
                'color' => $record['color'] ?? null,
                'size' => $record['size'] ?? null,
                'weight' => $record['weight'] ?? null,
                'price_cents' => $record['price_cents'] ?? null,
                'sale_price_cents' => $record['sale_price_cents'] ?? null,
                'cost_cents' => $record['cost_cents'] ?? null,
                'sku' => $record['sku'] ?? null,
                'length' => $record['length'] ?? null,
                'width' => $record['width'] ?? null,
                'height' => $record['height'] ?? null,
                'note' => $record['note'] ?? null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
