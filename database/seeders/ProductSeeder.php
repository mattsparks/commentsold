<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use League\Csv\Reader;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath(storage_path('csv/products.csv'), 'r');
        $csv->setHeaderOffset(0);

        foreach ($csv as $record) {
            $user = User::find($record['admin_id']);

            // No user? skip.
            if (!$user) {
                continue;
            }

            DB::table('products')->insert([
                'id' => $record['id'] ?? null,
                'admin_id' => $record['admin_id'] ?? null,
                'name' => $record['product_name'] ?? null,
                'description' => $record['description'] ?? null,
                'style' => $record['style'] ?? null,
                'brand' => $record['brand'] ?? null,
                'product_type' => $record['product_type'] ?? null,
                'url' => $record['url'] ?? null,
                'shipping_price' => $record['shipping_price'] ?? null,
                'note' => $record['note'] ?? null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
