<?php

namespace Database\Seeders;

use App\Models\Inventory;
use App\Models\Product;
use Carbon\Carbon;
use League\Csv\Reader;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath(storage_path('csv/orders.csv'), 'r');
        $csv->setHeaderOffset(0);

        foreach ($csv as $record) {
            $productId = Product::find($record['product_id']);
            $inventoryId = Inventory::find($record['inventory_id']);

            // Check that these resources exist.
            if (!$productId || !$inventoryId) {
                continue;
            }

            DB::table('orders')->insert([
                'id' => $record['id'] ?? null,
                'product_id' => $record['product_id'],
                'inventory_id' => $record['inventory_id'],
                'street_address' => $record['street_address'] ?? null,
                'city' => $record['city'] ?? null,
                'state' => $record['state'] ?? null,
                'country_code' => $record['country_code'] ?? null,
                'zip' => $record['zip'] ?? null,
                'phone_number' => $record['phone_number'] ?? null,
                'email' => $record['email'] ?? null,
                'name' => $record['name'] ?? null,
                'order_status' => $record['order_status'] ?? null,
                'payment_ref' => $record['payment_ref'] ?? null,
                'transaction_id' => $record['transaction_id'] ?? null,
                'payment_amt_cents' => $this->getCentAmount($record['payment_amt_cents']),
                'ship_charged_cents' => $this->getCentAmount($record['ship_charged_cents']),
                'ship_cost_cents' => $this->getCentAmount($record['ship_cost_cents']),
                'subtotal_cents' => $this->getCentAmount($record['subtotal_cents']),
                'total_cents' => $this->getCentAmount($record['total_cents']),
                'shipper_name' => $this->getCentAmount($record['shipper_name']),
                'payment_date' => $this->getCentAmount($record['payment_date']),
                'shipped_date' => $this->getCentAmount($record['shipped_date']),
                'tax_total_cents' => $this->getCentAmount($record['tax_total_cents']),
                'tracking_number' => $record['tracking_number'] ?? null,
                'payment_date' => $record['payment_date'] ?? null,
                'shipped_date' => $record['shipped_date'] ?? null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }

    /**
     * Get Cent Amount
     *
     * @param mixed $value
     * 
     * @return integer
     */
    private function getCentAmount($value)
    {
        return $value === '' || $value === 'NULL' ? 0 : $value;
    }
}
