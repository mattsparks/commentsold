## Install 

After pulling down the repo

```
composer install
npm install
npm run dev
php artisan migrate
php artisan db:seed
```

## Quick Overview

* The CSV data is parsed and imported into the database.
* During import the user's password is re-hased to allow it work with Laravel's authencation.
* Models Created:
    * User
    * Product
    * Inventory
    * Order
    * Plan
    * Shop
* Controllers Created:
    * DashboardController
    * OrderController
    * ProductController
* Requests Created (for authorization and validation):
    * AbstractProductRequest
    * DeleteProductRequest
    * EditProductRequest
    * StoreProductRequest
* Added a configuration file to hold shared validation rules.
* Created a simple money format class helper.
* A number of blade components were created for the views.

## A Few User Credientals
Just so you don't have to go fetch them:

| email                       | password   |
|-----------------------------|------------|
| kieth.loveall@blargmail.org | opFznlavmt |
| abby.sanden@foomail.org     | AeUkEfiLDt |
| margherita.vanhook@bar.com  | nYRpaFjUNM |
