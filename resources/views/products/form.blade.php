    <div class="flex flex-col mb-4">
        <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="name">Name</label>
        <input class="border py-2 px-3 text-grey-darkest" type="text" name="name" id="name" value="{{ old('name', $product->name ?? '') }}">
    </div>  

    <div class="flex flex-col mb-4">
        <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="name">Description</label>
        <input class="border py-2 px-3 text-grey-darkest" type="text" name="description" id="description" value="{{ old('description', $product->description ?? '') }}">
    </div>       

    <div class="flex flex-col mb-4">
        <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="style">Style</label>
        <input class="border py-2 px-3 text-grey-darkest" type="text" name="style" id="style" value="{{ old('style', $product->style ?? '') }}">
    </div>                          

    <div class="flex flex-col mb-4">
        <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="brand">Brand</label>
        <input class="border py-2 px-3 text-grey-darkest" type="text" name="brand" id="brand" value="{{ old('brand', $product->brand ?? '') }}">
    </div>   

    <div class="flex flex-col mb-4">
        <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="url">URL</label>
        <input class="border py-2 px-3 text-grey-darkest" type="text" name="url" id="url" value="{{ old('url', $product->url ?? '') }}">
    </div>   

    <div class="flex flex-col mb-4">
        <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="shipping_price">Shipping Price ($)</label>
        <input class="border py-2 px-3 text-grey-darkest" type="text" name="shipping_price" id="shipping_price" value="{{ old('shipping_price', $product->shipping_price ?? '') }}">
    </div>

    <div class="flex flex-col mb-4">
        <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="note">Note</label>
        <input class="border py-2 px-3 text-grey-darkest" type="text" name="note" id="note" value="{{ old('note', $product->note ?? '') }}">
    </div>