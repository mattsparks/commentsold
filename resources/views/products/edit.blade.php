<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Product ' . $product->name) }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    @if ($errors->any())
                        <div class="mb-8">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li class="text-red-700">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (Session::has('message'))
                        <p class="text-green-500">{{ Session::get('message') }}</p>
                    @endif

                    <form class="mb-6" action="{{ url('products/' . $product->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        @include('products.form')
                        <button class="block bg-black hover:bg-teal-dark text-white uppercase text-lg p-4 rounded" type="submit">Save</button>                                              
                </form>                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
