<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Orders') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="mb-8">
                        <p><strong>Total Sales For All Orders: ${{ $allTotal }}</strong></p>
                        <p><strong>Average Sale Total Accross All Orders: ${{ $avg }}</strong></p>
                    </div>
                    <div class="overflow-x-auto">
                        <table class="min-w-full divide-y divide-gray-200">
                            <thead class="bg-gray-50">
                                <tr>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Email Address</th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Product Name</th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Color</th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Size</th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Order Status</th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Total</th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Transaction ID</th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Shipper</th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Tracking Number</th>
                                    <th scope="col" class="relative px-6 py-3">
                                        <span class="sr-only">Edit</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                                @foreach ($orders as $order)
                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap">{{ $order->name ?? '' }}</td>
                                    <td class="px-6 py-4 whitespace-nowrap">{{ $order->email ?? '' }}</td>
                                    <td class="px-6 py-4 whitespace-nowrap">{{ $order->product->name ?? '' }}</td>
                                    <td class="px-6 py-4 whitespace-nowrap">{{ $order->inventory->color ?? '' }}</td>
                                    <td class="px-6 py-4 whitespace-nowrap">{{ $order->inventory->size ?? '' }}</td>
                                    <td class="px-6 py-4 whitespace-nowrap">{{ $order->order_status ?? '' }}</td>
                                    <td class="px-6 py-4 whitespace-nowrap">${{ \App\Helpers\MoneyFormat::toDollars($order->total_cents) ?? '' }}</td>
                                    <td class="px-6 py-4 whitespace-nowrap">{{ $order->transaction_id ?? '' }}</td>
                                    <td class="px-6 py-4 whitespace-nowrap">{{ $order->shipper_name ?? '' }}</td>
                                    <td class="px-6 py-4 whitespace-nowrap">{{ $order->tracking_number ?? '' }}</td>
                                </tr>
                                @endforeach                            
                            </tbody>
                        </table>                    
                    </div>
                    <div class="mt-8">
                        {{ $orders->links() }}
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
