<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="flex justify-between items-baseline">
                        <h2 class="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate mb-8">My Products</h2>
                        <a href="{{ url('products/create') }}" class="text-red-700">Add New</a>
                    </div>
                    @if (Session::has('success'))
                        <p class="text-green-500 mb-8">{{ Session::get('success') }}</p>
                    @endif                    
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                            <tr>
                                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Style</th>
                                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Brand</th>
                                <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Available SKUs</th>
                                <th scope="col" class="relative px-6 py-3">
                                    <span class="sr-only">Edit</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($products as $product)
                            <tr>
                                <td class="px-6 py-4 whitespace-nowrap">{{ $product->name }}</td>
                                <td class="px-6 py-4 whitespace-nowrap">{{ $product->style }}</td>
                                <td class="px-6 py-4 whitespace-nowrap">{{ $product->brand }}</td>
                                <td class="px-6 py-4 whitespace-nowrap">                                                                
                                    @foreach ($product->inventories as $inventory)
                                        <div class="mb-2 text-xs">
                                            {{ $inventory->sku }}
                                        </div>
                                    @endforeach
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                    <a href="{{ url('products/' . $product->id . '/edit') }}" class="text-indigo-600 hover:text-indigo-900">Edit</a>

                                    <form method="POST" action="{{ route('products.destroy', $product->id) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="text-red-700" onClick="confirm('Are ya sure?')">Delete</button>
                                    </form>                                                                    
                                </td>                                
                            </tr>
                            @endforeach                            
                        </tbody>
                    </table>
                    <div class="mt-8">
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
