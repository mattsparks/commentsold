<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * 
     * Super simple check to make sure the user owns the product.
     *
     * @return bool
     */
    public function authorize()
    {
        $productId = $this->route('product');
        $product = Product::findOrFail($productId);

        return $this->user()->id === $product->admin_id;
    }
}
