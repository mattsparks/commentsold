<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;

class DeleteProductRequest extends AbstractProductRequest
{
    /**
     * Get the validation rules that apply to the request.
     * 
     * No validation rules, we just want to check to make sure
     * the user owns the product.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
