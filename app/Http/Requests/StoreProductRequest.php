<?php

namespace App\Http\Requests;

class StoreProductRequest extends AbstractProductRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return config('validation.product');
    }
}
