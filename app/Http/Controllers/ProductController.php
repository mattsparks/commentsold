<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Helpers\MoneyFormat;
use App\Http\Requests\EditProductRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\DeleteProductRequest;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $product = new Product;
        $product->admin_id = Auth::user()->id;
        $product->name = $request->get('name');
        $product->description = $request->get('description');
        $product->product_type = 'clothing';
        $product->style = $request->get('style');
        $product->brand = $request->get('brand');
        $product->url = $request->get('url');
        $product->shipping_price = MoneyFormat::toCents($request->get('shipping_price'));
        $product->note = $request->get('note');
        $product->save();

        return redirect('/dashboard')->with('success', sprintf('Product %s Created!', $product->name));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return view('products.edit', [
            'product' => $product,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request\EditProductRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->name = $request->get('name');
        $product->description = $request->get('description');
        $product->style = $request->get('style');
        $product->brand = $request->get('brand');
        $product->url = $request->get('url');
        $product->shipping_price = MoneyFormat::toCents($request->get('shipping_price'));
        $product->note = $request->get('note');
        $product->save();

        return redirect('products/' . $product->id . '/edit')->with('message', 'Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);

        $product->delete();

        return redirect('dashboard')->with('success', 'Product Deleted!');
    }
}
