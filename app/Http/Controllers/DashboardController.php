<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Handle an incoming index request.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        $products = Product::where('admin_id', $user->id)->paginate(25);

        return view('dashboard', [
            'products' => $products,
        ]);
    }
}
