<?php

namespace App\Http\Controllers;

use App\Helpers\MoneyFormat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $total = $user->orders->sum('total_cents');
        $avg = round($total / $user->orders->count());

        return view('orders.index', [
            'orders' => $user->orders()->paginate(25),
            'allTotal' => MoneyFormat::toDollars($total),
            'avg' => MoneyFormat::toDollars($avg),
        ]);
    }
}
