<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\MoneyFormat;

class Product extends Model
{
    use HasFactory;

    protected $with = ['inventories'];

    /**
     * Get Shipping Price Attribute
     *
     * @param in $value
     * @return float
     */
    public function getShippingPriceAttribute($value): float
    {
        return MoneyFormat::toDollars($value);
    }

    /**
     * User
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Inventories
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }

    /**
     * Orders
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
