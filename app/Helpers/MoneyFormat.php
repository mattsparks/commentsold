<?php

namespace App\Helpers;

class MoneyFormat
{
    /**
     * toCents
     *
     * Transform a dollar amount to cents.
     * 
     * @param float $value
     * @return integer
     */
    public static function toCents(float $value): int
    {
        return $value * 100;
    }

    /**
     * toDollars
     * 
     * Transform cents to the dollar format.
     *
     * @param integer $value
     * @return float
     */
    public static function toDollars(int $value)
    {
        return number_format(($value / 100), 2, '.', ',');
    }
}
